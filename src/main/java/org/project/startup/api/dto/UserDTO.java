package org.project.startup.api.dto;

import javax.validation.constraints.Size;
import javax.validation.constraints.NotNull;
import org.hibernate.validator.constraints.Email;
import org.hibernate.validator.constraints.Length;

/**
 * 
 * @author ovidiu.dragoi
 *
 */
public class UserDTO implements DTOInterface {

	private long id;

	@Size(min = 5, max = 10)
	private String username;

	@Size(min = 5, max = 10)
	private String password;

	@Size(min = 5, max = 50)
	@Email
	private String email;

	@NotNull
	@Size(min = 5, max = 10)
	private String firstName;

	@Size(min = 5, max = 10)
	private String lastName;

	public UserDTO() {

	}

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public String getUsername() {
		return username;
	}

	public void setUsername(String username) {
		this.username = username;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(final String email) {
		this.email = email;
	}

	public String getFirstName() {
		return firstName;
	}

	public void setFirstName(final String firstName) {
		this.firstName = firstName;
	}

	public String getLastName() {
		return lastName;
	}

	public void setLastName(final String lastName) {
		this.lastName = lastName;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(final String password) {
		this.password = password;
	}
}
