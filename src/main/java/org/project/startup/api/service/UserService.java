package org.project.startup.api.service;

import java.io.UnsupportedEncodingException;
import java.util.List;
import org.project.startup.api.dto.UserDTO;
import org.project.startup.api.model.PasswordResetToken;
import org.project.startup.api.model.User;
import org.project.startup.api.model.VerificationRegistrationToken;

public interface UserService {
	List<User> findAll();

	UserDTO findById(long id);

	User save(UserDTO accountDto);

	UserDTO update(UserDTO accountDto);
	
	 void deleteUser(User user);

	User findByName(String project);

	User getUser(String verificationToken);

	boolean exists(User project);

	boolean emailExist(String email);

	void createVerificationTokenForUser(User user, String token);

	VerificationRegistrationToken getVerificationToken(String VerificationRegistrationToken);

	void saveRegisteredUser(User user);

	VerificationRegistrationToken generateNewVerificationToken(String token);

	void createPasswordResetTokenForUser(User user, String token);

	User findUserByEmail(String email);

	PasswordResetToken getPasswordResetToken(String token);

	User getUserByPasswordResetToken(String token);

	User getUserByID(long id);

	void changeUserPassword(User user, String password);

	boolean checkIfValidOldPassword(User user, String password);

	String validateVerificationToken(String token);

	List<String> getUsersFromSessionRegistry();
}
