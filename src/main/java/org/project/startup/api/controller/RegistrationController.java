package org.project.startup.api.controller;

import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;

import org.project.startup.api.dto.DTOInterface;
import org.project.startup.api.dto.MessageDTO;
import org.project.startup.api.dto.UserDTO;
import org.project.startup.api.event.RegistrationEvent;
import org.project.startup.api.event.ResendRegistrationTokenEvent;
import org.project.startup.api.model.User;
import org.project.startup.api.model.VerificationRegistrationToken;
import org.project.startup.api.service.UserService;
import org.project.startup.api.util.MessageType;
import org.project.startup.email.service.impl.EmailServiceImpl;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationEventPublisher;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;

@RestController
public class RegistrationController extends ApiController {

	@Autowired
	private UserService userService;

	@Autowired
	private ApplicationEventPublisher eventPublisher;

	private static final Logger logger = LoggerFactory.getLogger(SecurityController.class);

	@ApiOperation(value = "Register new user account", notes = "Creating a new user account", response = MessageDTO.class)
	@RequestMapping(value = "user/registration", method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
	@ApiResponses({ @ApiResponse(code = 200, message = "Success", response = UserDTO.class),
			@ApiResponse(code = 400, message = "Bad request", response = MessageDTO.class) })
	public ResponseEntity<DTOInterface> registerUser(@Validated @RequestBody final UserDTO dto,
			final HttpServletRequest request) {

		MessageDTO message = null;

		// logger.info("LOCALEEEE: " + request.getLocale() + "sfsdf" +
		// messageSource.getMessage("Size.userdto.firstName", new Object[] { "Ovidiu",
		// "2", "10" }, Locale.US));
		logger.info("Going to create a user");

		// save user
		final User registered = userService.save(dto);

		// user successfully saved publish event
		eventPublisher.publishEvent(new RegistrationEvent(registered, request.getLocale(), getAppUrl(request)));

		message = new MessageDTO(MessageType.SUCCESS, "User was saved");
		return new ResponseEntity<DTOInterface>(message, HttpStatus.OK);
	}

	@ApiOperation(value = "Confirm register user account based on confirmation token", notes = "Confirm Registration", response = MessageDTO.class)
	@RequestMapping(value = "user/registration-confirm", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
	@ApiResponses({ @ApiResponse(code = 200, message = "Success", response = UserDTO.class),
			@ApiResponse(code = 400, message = "Bad request", response = MessageDTO.class) })
	public ResponseEntity<DTOInterface> confirmRegistration(@RequestParam("token") final String token,
			final HttpServletRequest request) {

		final String result = userService.validateVerificationToken(token);
		logger.info("Going to verificate a token");
		if (result.equals("valid")) {
			MessageDTO message = new MessageDTO(MessageType.SUCCESS, result);
			return new ResponseEntity<DTOInterface>(message, HttpStatus.OK);
		}

		MessageDTO message = new MessageDTO(MessageType.ERROR, result);
		return new ResponseEntity<DTOInterface>(message, HttpStatus.BAD_REQUEST);
	}

	@ApiOperation(value = "Resend registration token", notes = "Resend token", response = MessageDTO.class)
	@RequestMapping(value = "user/resend-registration-token", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
	@ApiResponses({ @ApiResponse(code = 200, message = "Success", response = UserDTO.class),
			@ApiResponse(code = 400, message = "Bad request", response = MessageDTO.class) })
	public ResponseEntity<DTOInterface> resendRegistrationToken(@RequestParam("token") final String existingToken,
			final HttpServletRequest request) {

		final VerificationRegistrationToken newToken = userService.generateNewVerificationToken(existingToken);
		final User user = userService.getUser(newToken.getToken());
		
		// resend email with registration token
		eventPublisher.publishEvent(new ResendRegistrationTokenEvent(user, request.getLocale(), getAppUrl(request), newToken.getToken()));

		MessageDTO message = new MessageDTO(MessageType.SUCCESS, "token was resent");
		return new ResponseEntity<DTOInterface>(message, HttpStatus.OK);
	}

	@ApiOperation(value = "Reset user password", notes = "Reset lost user password", response = MessageDTO.class)
	@RequestMapping(value = "user/reset-password", method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
	@ApiResponses({ @ApiResponse(code = 200, message = "Success", response = UserDTO.class),
			@ApiResponse(code = 400, message = "Bad request", response = MessageDTO.class) })
	public ResponseEntity<DTOInterface> resetPassword(@Validated @RequestBody final UserDTO dto,
			final HttpServletRequest request) {

		// logger.info("LOCALEEEE: " + request.getLocale() + "sfsdf" +
		// messageSource.getMessage("Size.userdto.firstName", new Object[] { "Ovidiu",
		// "2", "10" }, Locale.US));
		logger.info("Going to create a user");

		// save user

		MessageDTO message = new MessageDTO(MessageType.SUCCESS, "User was saved");
		return new ResponseEntity<DTOInterface>(message, HttpStatus.OK);
	}
}
