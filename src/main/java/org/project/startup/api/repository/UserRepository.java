package org.project.startup.api.repository;

import java.util.List;

import javax.transaction.Transactional;

import org.project.startup.api.model.User;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

public interface UserRepository extends JpaRepository<User, Long> {
	
	User findById(long ID);
	
	User findByUsername(final String username);
	
	List<User> findAll();
 	
	//custom query
	@Query("select c from User c where email=?1")
	User findByEmail(final String email);

	@Transactional
	void removeUserByUsername(String username);
}
