# project-startup
https://www.concretepage.com/spring-4/spring-4-rest-cors-integration-using-crossorigin-annotation-xml-filter-example

oauth2 authentication:

it will be made a post to this url :
http://localhost:8080/NewProject/oauth/token?grant_type=password&username=ovidiu&password=parola
the user: is from the database table users
the password: the same


an basic authentication will be open for the client:
this user and password comes from oauth_client_details
as user: projectstartup
as password: parola

After the post you obtain the token to user in other normal requests 
you added in header:
Authorization: Bearer {token}

DOCKER:

Must be used Dockerfile and docker-compose.yml
a. startup-database-host container must be started before (the hostname is the name that also is used as db connection in datasource)
b. if any configuration is made to web container tomcat must stop,remove the container and also the images

Pay attention the db container (mysql) is initializing slowly and the web container doesn't manage to connect to database , it must be stoped and restarted (web)

docker build -t myapp --no-cache .
docker run -d  -p 8080:8080 --name mydockerapp myapp
localhost:8080/mydockerapp/
http://192.168.99.100:8080/NewProject/
logs: docker logs <container>

docker exec -t -i <container_name> /bin/bash
apt-get update
apt-get install vim

test mysql container remote
mysql -uroot -pparola86 -h localhost -P 6603


List all containers (only IDs)
docker ps -aq

Stop all running containers
docker stop $(docker ps -aq)

Remove all containers
docker rm $(docker ps -aq)

Remove all images
docker rmi $(docker images -q)

show networks:
docker network create --driver=bridge startup-db-network
docker network ls

check from inside container :
nmap -p 3306 startup-database-host

Eclipse Project:
Solution 1. This is always happened in deployment and debugging environment. In deployment environment, just make sure your server classpath has included the Spring jar library (e.g spring-4.0.4.jar).

In debugging environment, the steps may vary from different IDE, but the solution is same. In Eclipse, developers usually will create a tomcat, jboss…whatever application server for debugging, just make sure the correct Spring jars are included.

Double click on your debugging server Click on the “Open launch
configuration” to access the server environment Click on the classpath tab Include the Spring jar file here , it may also required common log jar due to Spring dependency.
Done, run your application again.
.

Solution 2. If you are using Maven as a build tool and downloading dependencies using it, there can be some jar conflict. Because Tomcat servers normally provide some jars such as servlet-api and jpa-api. So if you again included them using maven; there will be problems in identifying them.

Solution 3. If you are using Eclipse as your IDE and using Maven as your build tool and tomcat as your server; remember Tomcat server will not look on the jars which are just inside the folders. For that there's a small trick,

Right click on your project and select properties From the displayed window,

select Deployment Assembly

Select Add

Add Maven dependencies

Click Apply and Click OK

1:
Docker build with compose:
docker-compose up -d

if is not copying the war then
docker-compose up -d --build