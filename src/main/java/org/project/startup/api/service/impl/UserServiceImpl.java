package org.project.startup.api.service.impl;

import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;
import java.util.Calendar;
import java.util.UUID;

import org.project.startup.api.dto.UserDTO;
import org.project.startup.api.exception.RegistrationTokenNotFoundException;
import org.project.startup.api.exception.UserAlreadyExistsException;
import org.project.startup.api.model.PasswordResetToken;
import org.project.startup.api.model.User;
import org.project.startup.api.model.VerificationRegistrationToken;
import org.project.startup.api.repository.PasswordResetTokenRepository;
import org.project.startup.api.repository.RoleRepository;
import org.project.startup.api.repository.UserRepository;
import org.project.startup.api.repository.VerificationRegistrationTokenRepository;
import org.project.startup.api.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.session.SessionRegistry;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;
import org.springframework.security.crypto.password.PasswordEncoder;

@Service("userService")
public class UserServiceImpl implements UserService {

	@Autowired
	private UserRepository userRepository;

	@Autowired
	private RoleRepository roleRepository;

	@Autowired
	private PasswordResetTokenRepository passwordTokenRepository;

	@Autowired
	private VerificationRegistrationTokenRepository tokenRegistrationRepository;

	/*@Autowired
	private SessionRegistry sessionRegistry;
*/
	@Autowired
	private PasswordEncoder passwordEncoder;

	public static final String TOKEN_INVALID = "invalidToken";
	public static final String TOKEN_EXPIRED = "expired";
	public static final String TOKEN_VALID = "valid";

	@Override
	public List<User> findAll() {
		return userRepository.findAll();
	}

	@Override
	public UserDTO findById(long id) {
		User user = userRepository.findById(id);
		UserDTO userAccount = new UserDTO();
		userAccount.setEmail(user.getEmail());
		return userAccount;
	}

	@Override
	public User save(UserDTO accountDto) {

		if (emailExist(accountDto.getEmail())) {
			throw new UserAlreadyExistsException(accountDto.getEmail());
		}
		final User user = new User();

		BCryptPasswordEncoder encoder = new BCryptPasswordEncoder(11);

		user.setFirstName(accountDto.getFirstName());
		user.setLastName(accountDto.getLastName());
		user.setUsername(accountDto.getUsername());
		user.setPassword("{bcrypt}"+encoder.encode(accountDto.getPassword()));
		user.setEmail(accountDto.getEmail());
		user.setRoles(Arrays.asList(roleRepository.findByName("CLIENT")));
		User savedUser = userRepository.save(user);

		return savedUser;

	}

	@Override
	public void createVerificationTokenForUser(final User user, final String token) {
		final VerificationRegistrationToken myToken = new VerificationRegistrationToken(token, user);
		tokenRegistrationRepository.save(myToken);
	}

	@Override
	public User getUser(final String verificationToken) {
		final VerificationRegistrationToken token = tokenRegistrationRepository.findByToken(verificationToken);
		if (token != null) {
			return token.getUser();
		}
		return null;
	}

	@Override
	public void saveRegisteredUser(final User user) {
		userRepository.save(user);
	}

	@Override
	public VerificationRegistrationToken getVerificationToken(final String VerificationRegistrationToken) {
		return tokenRegistrationRepository.findByToken(VerificationRegistrationToken);
	}

	@Override
	public UserDTO update(UserDTO accountDto) {
		// TODO Auto-generated method stub
		UserDTO user = findById(accountDto.getId());
		user.setEmail(accountDto.getEmail());
		return findById(accountDto.getId());
	}

	@Override
	public User findByName(String project) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public boolean exists(User project) {
		// TODO Auto-generated method stub
		return false;
	}

    @Override
    public void deleteUser(final User user) {
        final VerificationRegistrationToken verificationToken = tokenRegistrationRepository.findByUser(user);

        if (verificationToken != null) {
        	tokenRegistrationRepository.delete(verificationToken);
        }

        final PasswordResetToken passwordToken = passwordTokenRepository.findByUser(user);

        if (passwordToken != null) {
            passwordTokenRepository.delete(passwordToken);
        }

        userRepository.delete(user);
}

	public boolean emailExist(final String email) {
		return userRepository.findByEmail(email) != null;
	}

	@Override
	public User getUserByPasswordResetToken(final String token) {
		return passwordTokenRepository.findByToken(token).getUser();
	}

	@Override
	public PasswordResetToken getPasswordResetToken(final String token) {
		return passwordTokenRepository.findByToken(token);
	}

	@Override
	public User findUserByEmail(final String email) {
		return userRepository.findByEmail(email);
	}

	@Override
	public User getUserByID(final long id) {
		return userRepository.findOne(id);
	}

	@Override
	public String validateVerificationToken(String token) {
		final VerificationRegistrationToken verificationToken = tokenRegistrationRepository.findByToken(token);
		if (verificationToken == null) {
			return TOKEN_INVALID;
		}

		final User user = verificationToken.getUser();
		final Calendar cal = Calendar.getInstance();
		if ((verificationToken.getExpiryDate().getTime() - cal.getTime().getTime()) <= 0) {
			tokenRegistrationRepository.delete(verificationToken);
			return TOKEN_EXPIRED;
		}

		user.setEnabled(true);
		// tokenRepository.delete(verificationToken);
		userRepository.save(user);
		return TOKEN_VALID;
	}

	@Override
	public void createPasswordResetTokenForUser(final User user, final String token) {
		final PasswordResetToken myToken = new PasswordResetToken(token, user);
		passwordTokenRepository.save(myToken);
	}
	
    @Override
    public VerificationRegistrationToken generateNewVerificationToken(final String existingVerificationToken) {
    	VerificationRegistrationToken vToken = tokenRegistrationRepository.findByToken(existingVerificationToken);
    	
    	if (vToken == null) {
    		throw new RegistrationTokenNotFoundException();
    	} 
    	
        vToken.updateToken(UUID.randomUUID().toString());
        vToken = tokenRegistrationRepository.save(vToken);
        return vToken;
}

	@Override
	public List<String> getUsersFromSessionRegistry() {
/*		return sessionRegistry.getAllPrincipals().stream()
				.filter((u) -> !sessionRegistry.getAllSessions(u, false).isEmpty()).map(Object::toString)
				.collect(Collectors.toList());*/
		return null;
	}

	@Override
	public boolean checkIfValidOldPassword(final User user, final String oldPassword) {
		return passwordEncoder.matches(oldPassword, user.getPassword());
	}

	@Override
	public void changeUserPassword(final User user, final String password) {
		user.setPassword(passwordEncoder.encode(password));
		userRepository.save(user);
	}
}