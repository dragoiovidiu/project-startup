package org.project.startup.api.config;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import springfox.documentation.swagger.web.ApiKeyVehicle;
import springfox.documentation.builders.ApiInfoBuilder;
import springfox.documentation.builders.ImplicitGrantBuilder;
import springfox.documentation.builders.OAuthBuilder;
import springfox.documentation.builders.PathSelectors;
import springfox.documentation.service.ApiInfo;
import springfox.documentation.service.GrantType;
import springfox.documentation.service.LoginEndpoint;
import springfox.documentation.service.SecurityScheme;
import springfox.documentation.spi.DocumentationType;
import springfox.documentation.spring.web.plugins.Docket;
import springfox.documentation.swagger.web.SecurityConfiguration;
import springfox.documentation.swagger2.annotations.EnableSwagger2;
import springfox.documentation.service.AuthorizationScope;

import static com.google.common.collect.Lists.newArrayList;

import static com.google.common.base.Predicates.*;
import static com.google.common.collect.Lists.*;
import static springfox.documentation.builders.PathSelectors.*;

import java.util.List;

@Configuration
@EnableSwagger2
public class SwaggerConfig {

	@Bean
	public Docket startupApi() {
		return new Docket(DocumentationType.SWAGGER_2).groupName("startup-api")
				.apiInfo(apiInfo()).select().paths(PathSelectors.any()).build();
	}

	private ApiInfo apiInfo() {
		return new ApiInfoBuilder().title("A start-up application using the spring mvc").description(
				"Please modify this API accordingly to the projects")
				.termsOfServiceUrl(
						"https://github.com/ovidiubrunet/startup-springmvc")
				.contact("Dragoi Ovidiu").license("Start-Up Group")
				.licenseUrl(
						"https://github.com/ovidiubrunet/startup-springmvc")
				.version("1.0").build();
	}
}
